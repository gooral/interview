package com.ubs.tools;

import java.util.List;

public class IntegerUtils {

	public Integer getSecondBiggest(List<Integer> integerList) throws IllegalArgumentException {
		int size = integerList.size();
		int biggest = Integer.MIN_VALUE;
		int secondBiggest = Integer.MIN_VALUE;

		if (size < 2) {
			throw new IllegalArgumentException("List is too small");
		}

		for (Integer integer : integerList) {
			if (integer > biggest) {
				secondBiggest = biggest;
				biggest = integer;
			}
			if (integer > secondBiggest && integer != biggest) {
				secondBiggest = integer;
			}
		}

		if (secondBiggest == Integer.MIN_VALUE) {
			throw new IllegalArgumentException("There is no second biggest");
		}

		return secondBiggest;
	}
}
