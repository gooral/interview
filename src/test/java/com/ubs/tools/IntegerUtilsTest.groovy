package com.ubs.tools

import spock.lang.Specification
import spock.lang.Unroll

class IntegerUtilsTest extends Specification {

    @Unroll
    def "Test getSecondBiggest method, input #input"() {
        given:
        IntegerUtils utils = new IntegerUtils()

        when:
        Integer result = utils.getSecondBiggest(input)

        then:
        result == expected

        where:
        input                                         || expected
        [1, 2, 3, 4]                                  || 3
        [7, 6, 5, 4]                                  || 6
        [8, 7]                                        || 7
        [66, 88, 77]                                  || 77
        [88, 88, 99, 99]                              || 88
        [-8, -9, -8, -10]                             || -9
        [-10, -10, 0, 2]                              || 0
        [-9, -9, 2, 2]                                || -9
        [Integer.MIN_VALUE, 0, 2, Integer.MIN_VALUE]  || 0
        [Integer.MAX_VALUE, 1, 2, Integer.MAX_VALUE]  || 2
        [Integer.MIN_VALUE, -1, 2, Integer.MAX_VALUE] || 2
    }

    @Unroll
    def "Test getSecondBiggest method to throw #exception, input: #input"() {
        given:
        IntegerUtils utils = new IntegerUtils()

        when:
        utils.getSecondBiggest(input)

        then:
        thrown exception

        where:
        input                   || exception
        [0]                     || IllegalArgumentException
        Collections.emptyList() || IllegalArgumentException
        ["a", "b", "c"]         || ClassCastException
        [5.0, 6.6]              || ClassCastException
        [2, 2, 2, 2]            || IllegalArgumentException
        [0, 0, 0]               || IllegalArgumentException
    }
}
